import Vue from 'vue';
import Router from 'vue-router';
import { ipcRenderer } from 'electron'; // eslint-disable-line
import path from 'path';

import EventBus from './event-bus';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default,
    },
    {
      path: '/settings',
      name: 'settings-page',
      component: require('@/components/SettingsPage').default,
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});

router.beforeEach((to, from, next) => {
  EventBus.$emit('loading', true).$emit('error', null);

  ipcRenderer.send('update-badge', null);

  const trayOptions = {
    path: path.join(__static, 'logo.png'),
    toolTip: 'tfs-notifier',
  };

  ipcRenderer.send('update-tray', trayOptions);
  next();
});

export default router;
