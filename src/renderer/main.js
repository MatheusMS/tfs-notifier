import Vue from 'vue';
import axios from 'axios';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faThumbtack,
  faCog, faArrowCircleLeft, faHome, faInfoCircle, faEye, faEyeSlash, faCaretDown, faCaretRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Config from 'electron-config';
import AutoLaunch from 'auto-launch';

import App from './App';
import router from './router';
import store from './store';

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));

const settings = new Config({
  defaults: {
    tfs: '',
    organization: '',
    projects: [],
    token: '',
    refreshInterval: 5, // seconds
    hiddenPullRequests: [],
  },
});

const autoLauncher = new AutoLaunch({ name: 'tfs-notifier' });

axios.defaults.baseURL = `${settings.get('tfs')}/${settings.get('organization')}`;

Vue.settings = Vue.prototype.$settings = settings;
Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;
Vue.autoLauncher = Vue.prototype.$autoLauncher = autoLauncher;

library.add([faThumbtack,
  faCog, faArrowCircleLeft, faHome, faInfoCircle, faEye, faEyeSlash, faCaretDown, faCaretRight]);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(BootstrapVue);

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>',
}).$mount('#app');
