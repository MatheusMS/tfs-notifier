import { app, ipcMain, BrowserWindow, Menu, Tray } from 'electron' // eslint-disable-line
import WindowStateKeeper from 'electron-window-state';
import Badge from 'electron-windows-badge';
import path from 'path';

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\') // eslint-disable-line
}

app.setAppUserModelId('com.autotrac.tfs-notifier');

let mainWindow;
let badge; // eslint-disable-line no-unused-vars
let tray = null;
const winURL = process.env.NODE_ENV === 'development'
  ? 'http://localhost:9080'
  : `file://${__dirname}/index.html`;

function createWindow() {
  /**
   * Initial window options
   */
  const height = 220;
  const width = 240;

  const mainWindowState = WindowStateKeeper({
    defaultWidth: width,
    defaultHeight: height,
  });

  mainWindow = new BrowserWindow({
    x: mainWindowState.x,
    y: mainWindowState.y,
    width: mainWindowState.width,
    height: mainWindowState.height,
    minHeight: height,
    minWidth: width,
    maximizable: false,
    autoHideMenuBar: true,
    alwaysOnTop: false,
    icon: path.join(__static, 'logo.png'),
    webPreferences: {
      nodeIntegration: true,
    },
  });

  mainWindow.loadURL(winURL);

  mainWindow.on('close', (event) => {
    if (!app.isQuitting) {
      event.preventDefault();
      mainWindow.hide();
    }
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
    tray.destroy();
  });

  tray = new Tray(path.join(__static, 'logo.png'));
  tray.setToolTip('tfs-notifier');
  tray.on('click', () => {
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show(); // eslint-disable-line
  });

  ipcMain.on('update-tray', (event, options) => {
    tray.setImage(options.path);
    tray.setToolTip(options.toolTip);
  });

  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Open',
      click: () => { mainWindow.show(); },
    },
    {
      label: 'Quit',
      click: () => {
        app.isQuitting = true;
        app.quit();
      },
    }]);

  tray.setContextMenu(contextMenu);

  mainWindowState.manage(mainWindow);

  badge = new Badge(mainWindow, {});
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
